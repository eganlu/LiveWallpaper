package com.mygdx.game.android;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.badlogic.gdx.backends.android.AndroidLiveWallpaperService;
import com.mygdx.game.LWP;
import com.mygdx.game.Resolver;

/**
 * Created by Luke Egan on 14/09/2015.
 */
public class LWP_Android extends AndroidLiveWallpaperService {
    public static float pixelOffset = 0;

    public static int level;
    public int scale;

    public static Intent batteryStatus;
    @Override
    public void onCreateApplication () {
        super.onCreateApplication();

        batteryStatus = registerReceiver(this.myBroadcastReceiver,  new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

        final AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
        config.useCompass = false;
        config.useWakelock = false;
        config.useAccelerometer = true;
        config.getTouchEventsForLiveWallpaper = false;

        final ApplicationListener listener = new WallpaperListener();
        initialize(listener, config);
    }

    private BroadcastReceiver myBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int level = intent.getIntExtra("level",0);
            int batteryPct = (int)(100*level / (float)scale);
            LWP_Android.this.level = batteryPct;
        }
    };

    public static class WallpaperListener extends LWP{
        @Override
        public void create() {
            super.resolver = new Resolver() {

                @Override
                public int getBatteryLevel() {
                    return level;
                }
            };

            super.create();
        };
    }

    @Override
    public void onDestroy() {
        unregisterReceiver(myBroadcastReceiver);
        super.onDestroy();
    }
}