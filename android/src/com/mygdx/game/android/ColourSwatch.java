package com.mygdx.game.android;

/**
 * Created by Luke Egan on 09/11/2015.
 */
public class ColourSwatch {

    private int innerCircleColour;
    private int outerCircleColour;
    private int backgroundColour;
    private int mID;

    public ColourSwatch(int innerCircleColour, int outerCircleColour, int backgroundColour, int ID) {
        this.innerCircleColour = innerCircleColour;
        this.outerCircleColour = outerCircleColour;
        this.backgroundColour = backgroundColour;
        this.mID = ID;
    }

    public int getID() {
        return mID;
    }

    public void setID(int mID) {
        this.mID = mID;
    }

    public int getInnerCircleColour() {
        return innerCircleColour;
    }

    public void setInnerCircleColour(int innerCircleColour) {
        this.innerCircleColour = innerCircleColour;
    }

    public int getOuterCircleColour() {
        return outerCircleColour;
    }

    public void setOuterCircleColour(int outerCircleColour) {
        this.outerCircleColour = outerCircleColour;
    }

    public int getBackgroundColour() {
        return backgroundColour;
    }

    public void setBackgroundColour(int backgroundColour) {
        this.backgroundColour = backgroundColour;
    }

}
