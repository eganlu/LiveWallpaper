package com.mygdx.game.android;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import com.lukeegan.livewallpaper.R;
import com.mygdx.game.Config;

public class Preferences extends AppCompatActivity {

    public ColourSwatch[] mColourSwatch = generateColourSwatches();

    private ColourSwatch[] generateColourSwatches() {
        ColourSwatch[] colourSwatch = new ColourSwatch[12];

        //below is the most boilerplate of boilerplate code, but it's seems extraneous to use xml just for this
        //i know how bad this part is -__-
        colourSwatch[0] = new ColourSwatch(
                0xfc560bff,
                0xfd772eff,
                0xffeb3bff,
                R.drawable.palette1
        );

        colourSwatch[1] = new ColourSwatch(
                0x3799ABFF,
                0xD6EEEEFF,
                0xFED475FF,
                R.drawable.palette2
        );

        colourSwatch[2] = new ColourSwatch(
                0x03a9f4ff,
                0x0288d1ff,
                0xffeb3bff,
                R.drawable.palette3
        );

        colourSwatch[3] = new ColourSwatch(
                0x03a9f4ff,
                0x0288d1ff,
                0x009688ff,
                R.drawable.palette4
        );

        colourSwatch[4] = new ColourSwatch(
                0x4caf50ff,
                0x0288d1ff,
                0x009688ff,
                R.drawable.palette5
        );

        colourSwatch[5] = new ColourSwatch(
                0x4caf50ff,
                0x388e3cff,
                0x009688ff,
                R.drawable.palette6
        );

        colourSwatch[6] = new ColourSwatch(
                0x9e9e9eff,
                0x455a64ff,
                0x658b9dff,
                R.drawable.palette7
        );

        colourSwatch[7] = new ColourSwatch(
                0xff4081ff,
                0xe64a19ff,
                0xff5722ff,
                R.drawable.palette8
        );

        colourSwatch[8] = new ColourSwatch(
                0xff4081ff,
                0x7b1fa2ff,
                0x448affff,
                R.drawable.palette9
        );

        colourSwatch[9] = new ColourSwatch(
                0xe1e1e1ff,
                0x5a5a5aff,
                0x919191ff,
                R.drawable.palette10
        );

        colourSwatch[10] = new ColourSwatch(
                0x8bc34aff,
                0x689f38ff,
                0xe1a84fff,
                R.drawable.palette11
        );

        colourSwatch[11] = new ColourSwatch(
                0x701919ff,
                0xb02f16ff,
                0xe1a84fff,
                R.drawable.palette12
        );

        return colourSwatch;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preferences);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Settings");
        }

        ImageAdapter adapter = new ImageAdapter(this, mColourSwatch);
        ExpandableHeightGridView gridView = (ExpandableHeightGridView) findViewById(R.id.gridViewOfImages);
        gridView.setExpanded(true);
        gridView.setAdapter(adapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Config.innerColor= mColourSwatch[position].getInnerCircleColour();
                Config.outerColor= mColourSwatch[position].getOuterCircleColour();
                Config.backGroundColor= mColourSwatch[position].getBackgroundColour();
                Config.save();
                Toast.makeText(Preferences.this, "Colour of wallpaper changed.", Toast.LENGTH_SHORT).show();
            }
        });

    }
}
