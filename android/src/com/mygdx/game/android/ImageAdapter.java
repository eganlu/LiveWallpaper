package com.mygdx.game.android;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;


public class ImageAdapter extends BaseAdapter {

    private Context mContext;
    private ColourSwatch mColourSwatch[];

    public ImageAdapter(Context c , ColourSwatch[] array) {
        mContext = c;
        mColourSwatch = array;
    }

    @Override
    public int getCount() {
        return mColourSwatch.length;
    }

    @Override
    public Object getItem(int i) {
        if (mColourSwatch != null)
            return mColourSwatch[i];
        else
            return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView = new ImageView(mContext);
        imageView.setImageResource(mColourSwatch[position].getID());
        imageView.setAdjustViewBounds(true);
        return imageView;
    }

}
