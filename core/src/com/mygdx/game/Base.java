package com.mygdx.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;



public class Base implements Screen {
	public Game game;
	public Resolver resolver;
	public GL20 gl;
	public float sW, sH;

	private long diff, start;
	private final float targetFPS = 25f; // 20-30 is enough
	private final long targetDelay = 1000 / (long) targetFPS;

	private int backgroundColor = 0xFED475FF;

	public Base(Game game, Resolver resolver) {
		this.game = game;
		this.resolver = resolver;
		this.gl = Gdx.gl20;
		if (Gdx.graphics.getHeight()>Gdx.graphics.getWidth()) {
			this.sW = 10f;
			this.sH = (Gdx.graphics.getHeight() / (float) Gdx.graphics.getWidth()) * 10f;
		} else {
			this.sH = 10f;
			this.sH = (Gdx.graphics.getWidth() / (float) Gdx.graphics.getHeight()) * 10f;
		}
	}

	public void render(float delta) {
		gl.glClearColor( ((backgroundColor & 0xff000000) >>> 24)/255f,
				((backgroundColor & 0x00ff0000) >>> 16)/255f,
				((backgroundColor & 0x0000ff00) >>> 8)/255f,
				(backgroundColor & 0x000000ff)/255f);
		gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
	}

	public void pause() {}
	public void resume() {}
	public void show() {}

	public void resize(int width, int height) {
		if (Gdx.graphics.getHeight()>Gdx.graphics.getWidth()) {
			this.sW = 10f;
			this.sH = (Gdx.graphics.getHeight() / (float) Gdx.graphics.getWidth()) * 10f;
		} else {
			this.sH = 10f;
			this.sW = (Gdx.graphics.getWidth() / (float) Gdx.graphics.getHeight()) * 10f;
		}
	}

	public void hide() {}
	public void dispose() {}

	public void limitFPS() {
		diff = System.currentTimeMillis() - start;

		if (diff < targetDelay) {
			try {
				Thread.sleep(targetDelay - diff);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		start = System.currentTimeMillis();
	}

	public void setBackgroundColor(int backgroundColor) {
		this.backgroundColor = backgroundColor;
	}
}
