package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;

public class Config {
	public static final Preferences preferences = Gdx.app.getPreferences("preferences");

	public static final String IS_PARALLAX = "isParallax";
	public static final String INNER_CIRCLE_COLOR_KEY = "innerCircleColor";
	public static final String OUTER_CIRCLE_COLOR_KEY = "outerCircleColor";
	public static final String BACKGROUND_COLOR_KEY = "backgroundColor";

	public static boolean isParallax = false;
	public static int innerColor = 0x3799ABFF;
	public static int outerColor = 0xD6EEEEFF;
	public static int backGroundColor = 0xFED475FF;

	public static void load() {
		if (!preferences.contains(INNER_CIRCLE_COLOR_KEY) ||
				!preferences.contains(OUTER_CIRCLE_COLOR_KEY) ||
				!preferences.contains(BACKGROUND_COLOR_KEY) ||
				!preferences.contains(IS_PARALLAX)) {
			save();
		} else {
			isParallax = preferences.getBoolean(IS_PARALLAX, false);
			innerColor = preferences.getInteger(INNER_CIRCLE_COLOR_KEY, 0x3799ABFF);
			outerColor = preferences.getInteger(OUTER_CIRCLE_COLOR_KEY, 0xD6EEEEFF);
			backGroundColor = preferences.getInteger(BACKGROUND_COLOR_KEY, 0xFED475FF);
		}
	}

	public static void save() {
		preferences.putBoolean(IS_PARALLAX, isParallax);
		preferences.putInteger(INNER_CIRCLE_COLOR_KEY, innerColor);
		preferences.putInteger(OUTER_CIRCLE_COLOR_KEY, outerColor);
		preferences.putInteger(BACKGROUND_COLOR_KEY, backGroundColor);
		preferences.flush();
	}
}
