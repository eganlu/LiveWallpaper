package com.mygdx.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.ChainShape;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;


@SuppressWarnings("FieldCanBeLocal")
public class Main extends Base {

    BodyDef bodyDef;
    FixtureDef ballFixtureDef ;


    private int batteryLevel;
    private float outerRadius = 4.5f;

    //constants
    private final float TIMESTEP = 1/25f;
    private final int VELOCITYITERATIONS = 4, POSITIONITERATIONS = 1;

    //camera and world
    private OrthographicCamera camera;
    private SpriteBatch batch;
    private SpriteBatch UIBatch;
    private World world;

    //text
    private BitmapFont font;
    private GlyphLayout layoutOfNumber;
    private GlyphLayout textAmountBoundary;

    //bodies and sprites
    private Vector2 movement = new Vector2();
    private Body innerBall;
    private Body outerBall;
    private Sprite innerSprite;
    private Sprite outerSprite;
    private Array<Body> tmpBodies = new Array<Body>();

    public Main(Game game, Resolver resolver) {
        super(game, resolver);
    }

    @Override
    public void show() {
        Config.load();
        setBackgroundColor(Config.backGroundColor);
        UIBatch = new SpriteBatch();
        batch = new SpriteBatch(10);
        world = new World(new Vector2(0,0),true);

        batteryLevel = resolver.getBatteryLevel();

        bodyDef = new BodyDef();
        ballFixtureDef = new FixtureDef();

        // outer circle start
        generateOuterBall(bodyDef, ballFixtureDef, outerRadius);

        //inner ball body definition
        generateInnerBall(bodyDef, ballFixtureDef);

        resetCamera();

        font = new BitmapFont(Gdx.files.internal("myfont.fnt"));
        layoutOfNumber = new GlyphLayout();
        layoutOfNumber.setText(font, "00 %");
        textAmountBoundary = new GlyphLayout();
        textAmountBoundary.setText(font,"Battery Level:");

    }

    @Override
    public void resume() {
        super.resume();
        Config.load();
        setBackgroundColor(Config.backGroundColor);
        outerSprite.setColor(new Color(Config.outerColor));
        batteryLevel = resolver.getBatteryLevel();
        world.getBodies(tmpBodies);
        world.destroyBody(tmpBodies.get(1));
        generateInnerBall(bodyDef,ballFixtureDef);
    }

    @Override
    public void render(float delta) {
        float xDirection = Gdx.input.getAccelerometerX();
        float yDirection = Gdx.input.getAccelerometerY();
        movement.set(-xDirection, -yDirection);
        innerBall.applyForceToCenter(movement, true);
        world.step(TIMESTEP, VELOCITYITERATIONS, POSITIONITERATIONS);

        draw(delta); // Main draw part, all of the rendering

        limitFPS(); //Optional way to limit the fps of the live wallpaper
    }

    private void draw(float delta) {
        super.render(delta);

        camera.update();
        batch.setProjectionMatrix(camera.combined);

        batch.begin();
        world.getBodies(tmpBodies);
        for (Body body : tmpBodies) {
            if (body.getUserData() != null && body.getUserData() instanceof Sprite) {
                Sprite sprite = (Sprite) body.getUserData();
                sprite.setRotation(body.getAngle()* MathUtils.radiansToDegrees);
                sprite.setPosition(body.getPosition().x - sprite.getWidth() / 2, body.getPosition().y - sprite.getHeight() / 2);
                sprite.draw(batch);
            }
        }
        batch.end();

        UIBatch.begin();
        font.draw(UIBatch, "Battery Level:", Gdx.graphics.getWidth()/2 - textAmountBoundary.width/2,
                Gdx.graphics.getHeight()/2 + textAmountBoundary.height*2);
        font.draw(UIBatch , batteryLevel + " %" ,Gdx.graphics.getWidth()/2 - layoutOfNumber.width/2,
                Gdx.graphics.getHeight()/2);
        UIBatch.end();

    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        resetCamera();
    }

    @Override
    public void dispose() {
        batch.dispose();
        font.dispose();
        world.dispose();
    }

    private void resetCamera() {
        camera = new OrthographicCamera(sW, sH);
        camera.setToOrtho(false, sW, sH);
        camera.position.set(0, 0, 0);
    }

    private void generateInnerBall(BodyDef bodyDef, FixtureDef ballFixtureDef) {
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        bodyDef.position.set(0,0);

        //inner ball shape
        CircleShape ballShape = new CircleShape();
        float innerRadius = (outerRadius*batteryLevel)/100f;
        ballShape.setRadius(innerRadius);

        //inner ball fixture definition

        ballFixtureDef.shape = ballShape;
        ballFixtureDef.density = 0.1f/innerRadius;
        ballFixtureDef.friction = 0.25f;
        ballFixtureDef.restitution = 0.75f;

        innerBall = world.createBody(bodyDef);
        innerBall.createFixture(ballFixtureDef);

        innerSprite = new Sprite(new Texture("doge512.png"));
        innerSprite.setSize(innerRadius * 2, innerRadius * 2);
        innerSprite.setOrigin(innerSprite.getWidth() / 2, innerSprite.getHeight() / 2);
        //innerSprite.setColor(new Color(Config.innerColor));
        innerBall.setUserData(innerSprite);

        ballShape.dispose();

    }

    private void generateOuterBall(BodyDef bodyDef, FixtureDef ballFixtureDef, float radius) {
        bodyDef.type = BodyDef.BodyType.StaticBody;
        bodyDef.position.set(0,0);

        ChainShape circleShape = new ChainShape();
        int amountOfSides = 32;
        float x = 0, y =0;
        Vector2[] vertices = getCircleVertices(amountOfSides, radius, x, y);
        circleShape.createLoop(vertices);

        ballFixtureDef.shape = circleShape;
        ballFixtureDef.friction = 0.5f;
        ballFixtureDef.restitution = 0.1f;

        outerBall = world.createBody(bodyDef);
        outerBall.createFixture(ballFixtureDef);

        outerSprite = new Sprite(new Texture("texture512.png"));
        outerSprite.setSize(radius * 2, radius * 2);
        outerSprite.setOrigin(outerSprite.getWidth() / 2, outerSprite.getHeight() / 2);
        outerSprite.setColor(new Color(Config.outerColor));
        outerBall.setUserData(outerSprite);


        circleShape.dispose();

    }

    private Vector2[] getCircleVertices(int amountOfSides, float radius, float x, float y) {
        Vector2[] array = new Vector2[amountOfSides];
        float angle = (float) (2*Math.PI /((float) amountOfSides));
        float currentAngle = 0;
        for (int index = 0; index <amountOfSides; index++) {
            float xLocation = (float) (x +(radius*Math.cos(currentAngle)));
            float yLocation = (float) (y +(radius*Math.sin(currentAngle)));
            Vector2 current = new Vector2( xLocation, yLocation);
            currentAngle+=angle;
            array[index] = current;
        }

        return array;
    }

}
